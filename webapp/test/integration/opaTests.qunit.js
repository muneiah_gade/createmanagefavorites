/* global QUnit */

QUnit.config.autostart = false;

sap.ui.getCore().attachInit(function() {
	"use strict";

	sap.ui.require([
		"be/xperthis/createmanagefavorites/test/integration/AllJourneys"
	], function() {
		QUnit.start();
	});
});