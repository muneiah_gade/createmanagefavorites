/*global location */
sap.ui.define([
	"./BaseController",
	"sap/ui/model/json/JSONModel",
	"../model/formatter",
	"sap/m/library",
	"sap/m/MessageToast"
], function (BaseController, JSONModel, formatter, mobileLibrary, MessageToast) {
	"use strict";

	// shortcut for sap.m.URLHelper
	var URLHelper = mobileLibrary.URLHelper;

	return BaseController.extend("be.xperthis.createmanagefavorites.controller.Detail", {

		formatter: formatter,

		/* =========================================================== */
		/* lifecycle methods                                           */
		/* =========================================================== */

		onInit : function () {
			// Model used to manipulate control states. The chosen values make sure,
			// detail page is busy indication immediately so there is no break in
			// between the busy indication for loading the view's meta data
			var oViewModel = new JSONModel({
				busy : false,
				delay : 0,
				lineItemListTitle : this.getResourceBundle().getText("detailLineItemTableHeading")
			});

			this.getRouter().getRoute("object").attachPatternMatched(this._onObjectMatched, this);

			this.setModel(oViewModel, "detailView");

			this.getOwnerComponent().getModel().metadataLoaded().then(this._onMetadataLoaded.bind(this));
		},

		/* =========================================================== */
		/* event handlers                                              */
		/* =========================================================== */

		/**
		 * Event handler when the share by E-Mail button has been clicked
		 * @public
		 */
		onSendEmailPress : function () {
			var oViewModel = this.getModel("detailView");

			URLHelper.triggerEmail(
				null,
				oViewModel.getProperty("/shareSendEmailSubject"),
				oViewModel.getProperty("/shareSendEmailMessage")
			);
		},

		/**
		 * Event handler when the share in JAM button has been clicked
		 * @public
		 */
		onShareInJamPress : function () {
			var oViewModel = this.getModel("detailView"),
				oShareDialog = sap.ui.getCore().createComponent({
					name : "sap.collaboration.components.fiori.sharing.dialog",
					settings : {
						object :{
							id : location.href,
							share : oViewModel.getProperty("/shareOnJamTitle")
						}
					}
				});

			oShareDialog.open();
		},

		/**
		 * Updates the item count within the line item table's header
		 * @param {object} oEvent an event containing the total number of items in the list
		 * @private
		 */
		onListUpdateFinished : function (oEvent) {
			var sTitle,
				iTotalItems = oEvent.getParameter("total"),
				oViewModel = this.getModel("detailView");

			// only update the counter if the length is final
			if (this.byId("lineItemsList").getBinding("items").isLengthFinal()) {
				if (iTotalItems) {
					sTitle = this.getResourceBundle().getText("detailLineItemTableHeadingCount", [iTotalItems]);
				} else {
					//Display 'Line Items' instead of 'Line items (0)'
					sTitle = this.getResourceBundle().getText("detailLineItemTableHeading");
				}
				oViewModel.setProperty("/lineItemListTitle", sTitle);
			}
		},

		/* =========================================================== */
		/* begin: internal methods                                     */
		/* =========================================================== */

		/**
		 * Binds the view to the object path and expands the aggregated line items.
		 * @function
		 * @param {sap.ui.base.Event} oEvent pattern match event in route 'object'
		 * @private
		 */
		_onObjectMatched : function (oEvent) {
			var sObjectId =  oEvent.getParameter("arguments").objectId;
			this.getModel("appView").setProperty("/layout", "TwoColumnsMidExpanded");
			this.getModel().metadataLoaded().then( function() {
				var sObjectPath = this.getModel().createKey("Suppliers", {
					SupplierID :  sObjectId
				});
				this._bindView("/" + sObjectPath);
			}.bind(this));
		},

		/**
		 * Binds the view to the object path. Makes sure that detail view displays
		 * a busy indicator while data for the corresponding element binding is loaded.
		 * @function
		 * @param {string} sObjectPath path to the object to be bound to the view.
		 * @private
		 */
		_bindView : function (sObjectPath) {
			// Set busy indicator during view binding
			var oViewModel = this.getModel("detailView");

			// If the view was not bound yet its not busy, only if the binding requests data it is set to busy again
			oViewModel.setProperty("/busy", false);

			this.getView().bindElement({
				path : sObjectPath,
				events: {
					change : this._onBindingChange.bind(this),
					dataRequested : function () {
						oViewModel.setProperty("/busy", true);
					},
					dataReceived: function () {
						oViewModel.setProperty("/busy", false);
					}
				}
			});
		},

		_onBindingChange : function () {
			var oView = this.getView(),
				oElementBinding = oView.getElementBinding();

			// No data for the binding
			if (!oElementBinding.getBoundContext()) {
				this.getRouter().getTargets().display("detailObjectNotFound");
				// if object could not be found, the selection in the master list
				// does not make sense anymore.
				this.getOwnerComponent().oListSelector.clearMasterListSelection();
				return;
			}

			var sPath = oElementBinding.getPath(),
				oResourceBundle = this.getResourceBundle(),
				oObject = oView.getModel().getObject(sPath),
				sObjectId = oObject.SupplierID,
				sObjectName = oObject.ContactName,
				oViewModel = this.getModel("detailView");

			this.getOwnerComponent().oListSelector.selectAListItem(sPath);

			oViewModel.setProperty("/saveAsTileTitle",oResourceBundle.getText("shareSaveTileAppTitle", [sObjectName]));
			oViewModel.setProperty("/shareOnJamTitle", sObjectName);
			oViewModel.setProperty("/shareSendEmailSubject", oResourceBundle.getText("shareSendEmailObjectSubject", [sObjectId]));
			oViewModel.setProperty("/shareSendEmailMessage", oResourceBundle.getText("shareSendEmailObjectMessage", [sObjectName, sObjectId, location.href]));
				
				
		},

		_onMetadataLoaded : function () {
			// Store original busy indicator delay for the detail view
			var iOriginalViewBusyDelay = this.getView().getBusyIndicatorDelay(),
				oViewModel = this.getModel("detailView"),
				oLineItemTable = this.byId("lineItemsList"),
				iOriginalLineItemTableBusyDelay = oLineItemTable.getBusyIndicatorDelay();

			// Make sure busy indicator is displayed immediately when
			// detail view is displayed for the first time
			oViewModel.setProperty("/delay", 0);
			oViewModel.setProperty("/lineItemTableDelay", 0);

			oLineItemTable.attachEventOnce("updateFinished", function() {
				// Restore original busy indicator delay for line item table
				oViewModel.setProperty("/lineItemTableDelay", iOriginalLineItemTableBusyDelay);
			});

			// Binding the view will set it to not busy - so the view is always busy if it is not bound
			oViewModel.setProperty("/busy", true);
			// Restore original busy indicator delay for the detail view
			oViewModel.setProperty("/delay", iOriginalViewBusyDelay);
			
			/* Hide the Edit, Save & Cancel Favorite List Items Buttons */
			this.toggleButton("toggleEditButton", true);
			this.toggleButton("toggleSaveButton", false);
			this.toggleButton("toggleCancelButton", false);
			this.toggleButton("toggleAddListItemButton", false);
			this.toggleInputEditable("toggleInputEditable", false);
			this.toggleInputEditable("toggleFavoriteInputEditable", false);
		},

		/**
		 * Set the full screen mode to false and navigate to master page
		 */
		onCloseDetailPress: function () {
			this.getModel("appView").setProperty("/actionButtonsInfo/midColumn/fullScreen", false);
			// No item should be selected on master after detail page is closed
			this.getOwnerComponent().oListSelector.clearMasterListSelection();
			this.getRouter().navTo("master");
		},

		/**
		 * Toggle between full and non full screen mode.
		 */
		toggleFullScreen: function () {
			var bFullScreen = this.getModel("appView").getProperty("/actionButtonsInfo/midColumn/fullScreen");
			this.getModel("appView").setProperty("/actionButtonsInfo/midColumn/fullScreen", !bFullScreen);
			if (!bFullScreen) {
				// store current layout and go full screen
				this.getModel("appView").setProperty("/previousLayout", this.getModel("appView").getProperty("/layout"));
				this.getModel("appView").setProperty("/layout", "MidColumnFullScreen");
			} else {
				// reset to previous layout
				this.getModel("appView").setProperty("/layout",  this.getModel("appView").getProperty("/previousLayout"));
			}
		},
		/**
			* Method to Open the Favorite List Item Dialog
		*/
		onOpenFavoriteListItemDialog: function(){
			var itemData = {MANDT:"", GUID:"", PRODUCT_ID:"", QUANTITY:"", UNIT:""};
			this._addFavoriteListItemDialog = sap.ui.xmlfragment("be.xperthis.createmanagefavorites.fragments.addFavoriteListItem", this);
			this.getView().addDependent(this._addFavoriteListItemDialog);
			var model = new JSONModel();
			model.setData({favoriteListItemSet: itemData});
			this._addFavoriteListItemDialog.setModel(model,"addFavoriteListItem");
			this._addFavoriteListItemDialog.bindElement("addFavoriteListItem>/favoriteListItemSet");
			this._addFavoriteListItemDialog.open();	
		},
		/**
			* Method to Add the Favorite List Item
		*/
		onAddFavoriteListItem: function(){
			this._addFavoriteListItemDialog.close();
		},
		/**
			* Method to Close the Favorite List Item Dialog
		*/
		onCloseFavoriteListItem: function(){
			this._addFavoriteListItemDialog.close();
		},
		/**
			* Method to Toggle the Button Visibility
			* @param {string} ModelName to  the Element .
			* @param {boolean} value defines the control visibility
		*/
		toggleInputEditable:function(modelName, value){
			var model = new JSONModel();
			model.setData({toggleEditableSet :{editable: value}});
			this.getView().setModel(model, modelName);
			this.getView().bindElement(modelName + ">/toggleEditableSet");
		},
		/**
			* Method to Toggle the Input field Editable property
			* ModelName
			* @param {string} ModelName to  the Element.
			* @param {boolean} value defines the control editable
		*/
		toggleButton:function(modelName, value){
			var model = new JSONModel();
			model.setData({toggleButtonSet :{visible: value}});
			this.getView().setModel(model, modelName);
			this.getView().bindElement(modelName + ">/toggleButtonSet");
		},
		/**
			* Method to Edit the Favorite Master List Items
		*/
		onEditFavoriteListItems: function(){
			this.toggleButton("toggleEditButton", false);
			this.toggleButton("toggleSaveButton", true);
			this.toggleButton("toggleCancelButton", true);
			this.toggleInputEditable("toggleInputEditable", true);
			this.toggleButton("toggleAddListItemButton", true);
			this.toggleInputEditable("toggleFavoriteInputEditable", true);
		},
		/**
			* Method to Save the Favorite Master List Items
		*/
		onSaveFavoriteListItems: function(){
			this.toggleButton("toggleEditButton", true);
			this.toggleButton("toggleSaveButton", false);
			this.toggleButton("toggleCancelButton", false);
			this.toggleInputEditable("toggleInputEditable", false);
			this.toggleButton("toggleAddListItemButton", false);
			this.toggleInputEditable("toggleFavoriteInputEditable", false);
		},
		/**
			* Method to Cancel the Favorite Master List Items
		*/
		onCancelFavoriteListItems: function(){
			this.toggleButton("toggleEditButton", true);
			this.toggleButton("toggleSaveButton", false);
			this.toggleButton("toggleCancelButton", false);
			this.toggleInputEditable("toggleInputEditable", false);
			this.toggleButton("toggleAddListItemButton", false);
			this.toggleInputEditable("toggleFavoriteInputEditable", false);
		},
		/**
			* Method to Create PR
		*/
		onCreatePR: function(){
			MessageToast.show("You will be re-directed to Create PR application");
		},
		/**
			* Method to Delete Favorite Item
		*/
		onDeleteFavoriteListItems: function(){
			
		}
		
		
		
	});

});