sap.ui.define([
	"./BaseController"
], function (BaseController) {
	"use strict";

	return BaseController.extend("be.xperthis.createmanagefavorites.controller.DetailObjectNotFound", {});
});